# obj-m += hello-1.o
# obj-m += hello-4.o
# obj-m += chardev.o
# obj-m += chardev-2.o
# obj-m += query_ioctl.o
# all:
# 	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules


# If KERNELRELEASE is defined, we've been invoked from the
# kernel build system and can use its language.
ifneq ($(KERNELRELEASE),)
	obj-m += poll.o
	# Otherwise we were called directly from the command
	# line; invoke the kernel build system.
else
	KERNELDIR ?= /lib/modules/$(shell uname -r)/build
	PWD := $(shell pwd)

default:
	$(MAKE) -C $(KERNELDIR) M=$(PWD) modules

clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean

endif

