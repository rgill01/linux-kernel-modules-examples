# linux-kernel-modules
Playing around with linux kernel module programming

# solus
- eopkg install system.devel 
- eopkg install linux-lts-headers
- note: uname -r is behind the latest linus-lts-headers.. so hacked Makefile for now..

# loading
- modinfo hello-*.ko for info
- insmod ./hello-1.ko  to load
- check in /proc/modules
- msgs go to journalctl -ra --system 
- rmmod hello-1 to remove

## char drivers
- for char driver: mknod /dev/xxxx c/b (major) (minor)
- remove with rm /dev/xxxx (dont forget to rmmod the module)
- OR: use cdev and dev to automatically mount to /dev, see query ex.. Then need to sudo chmod 666 /dev/xxxx

# source
-http://www.tldp.org/LDP/lkmpg/2.6/html/lkmpg.html#AEN121
-https://opensourceforu.com/tag/linux-device-drivers-series/